///////////////////////////////////////////////////////////////////////////////
// PulleyKinematicsTransformation.h

#pragma once

#include "ObjClassFactory.h"

class CPulleyKinematicsTransformationClassFactory : public CObjClassFactory
{
public:
	CPulleyKinematicsTransformationClassFactory();
	DECLARE_CLASS_MAP()
};


