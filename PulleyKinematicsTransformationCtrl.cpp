// PulleyKinematicsTransformationCtrl.cpp : Implementation of CTcPulleyKinematicsTransformationCtrl
#include "TcPch.h"
#pragma hdrstop

#include "PulleyKinematicsTransformationW32.h"
#include "PulleyKinematicsTransformationCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CPulleyKinematicsTransformationCtrl

CPulleyKinematicsTransformationCtrl::CPulleyKinematicsTransformationCtrl() 
	: ITcOCFCtrlImpl<CPulleyKinematicsTransformationCtrl, CPulleyKinematicsTransformationClassFactory>() 
{
}

CPulleyKinematicsTransformationCtrl::~CPulleyKinematicsTransformationCtrl()
{
}

