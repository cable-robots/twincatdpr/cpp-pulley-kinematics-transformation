///////////////////////////////////////////////////////////////////////////////
// PulleyKinematicsTransformationCtrl.h

#ifndef __PULLEYKINEMATICSTRANSFORMATIONCTRL_H__
#define __PULLEYKINEMATICSTRANSFORMATIONCTRL_H__

#include <atlbase.h>
#include <atlcom.h>


#include "resource.h"       // main symbols
#include "PulleyKinematicsTransformationW32.h"
#include "TcBase.h"
#include "PulleyKinematicsTransformationClassFactory.h"
#include "TcOCFCtrlImpl.h"

class CPulleyKinematicsTransformationCtrl 
	: public CComObjectRootEx<CComMultiThreadModel>
	, public CComCoClass<CPulleyKinematicsTransformationCtrl, &CLSID_PulleyKinematicsTransformationCtrl>
	, public IPulleyKinematicsTransformationCtrl
	, public ITcOCFCtrlImpl<CPulleyKinematicsTransformationCtrl, CPulleyKinematicsTransformationClassFactory>
{
public:
	CPulleyKinematicsTransformationCtrl();
	virtual ~CPulleyKinematicsTransformationCtrl();

DECLARE_REGISTRY_RESOURCEID(IDR_PULLEYKINEMATICSTRANSFORMATIONCTRL)
DECLARE_NOT_AGGREGATABLE(CPulleyKinematicsTransformationCtrl)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CPulleyKinematicsTransformationCtrl)
	COM_INTERFACE_ENTRY(IPulleyKinematicsTransformationCtrl)
	COM_INTERFACE_ENTRY(ITcCtrl)
	COM_INTERFACE_ENTRY(ITcCtrl2)
END_COM_MAP()

};

#endif // #ifndef __PULLEYKINEMATICSTRANSFORMATIONCTRL_H__
