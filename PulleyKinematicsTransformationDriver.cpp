///////////////////////////////////////////////////////////////////////////////
// PulleyKinematicsTransformationDriver.cpp
#include "TcPch.h"
#pragma hdrstop

#include "PulleyKinematicsTransformationDriver.h"
#include "PulleyKinematicsTransformationClassFactory.h"

DECLARE_GENERIC_DEVICE(PULLEYKINEMATICSTRANSFORMATIONDRV)

IOSTATUS CPulleyKinematicsTransformationDriver::OnLoad( )
{
	TRACE(_T("CObjClassFactory::OnLoad()\n") );
	m_pObjClassFactory = new CPulleyKinematicsTransformationClassFactory();

	return IOSTATUS_SUCCESS;
}

VOID CPulleyKinematicsTransformationDriver::OnUnLoad( )
{
	delete m_pObjClassFactory;
}

unsigned long _cdecl CPulleyKinematicsTransformationDriver::PULLEYKINEMATICSTRANSFORMATIONDRV_GetVersion( )
{
	return( (PULLEYKINEMATICSTRANSFORMATIONDRV_Major << 8) | PULLEYKINEMATICSTRANSFORMATIONDRV_Minor );
}

