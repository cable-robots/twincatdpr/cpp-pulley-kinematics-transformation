///////////////////////////////////////////////////////////////////////////////
// PulleyKinematicsTransformationDriver.h

#ifndef __PULLEYKINEMATICSTRANSFORMATIONDRIVER_H__
#define __PULLEYKINEMATICSTRANSFORMATIONDRIVER_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TcBase.h"

#define PULLEYKINEMATICSTRANSFORMATIONDRV_NAME        "PULLEYKINEMATICSTRANSFORMATION"
#define PULLEYKINEMATICSTRANSFORMATIONDRV_Major       1
#define PULLEYKINEMATICSTRANSFORMATIONDRV_Minor       0

#define DEVICE_CLASS CPulleyKinematicsTransformationDriver

#include "ObjDriver.h"

class CPulleyKinematicsTransformationDriver : public CObjDriver
{
public:
	virtual IOSTATUS	OnLoad();
	virtual VOID		OnUnLoad();

	//////////////////////////////////////////////////////
	// VxD-Services exported by this driver
	static unsigned long	_cdecl PULLEYKINEMATICSTRANSFORMATIONDRV_GetVersion();
	//////////////////////////////////////////////////////
	
};

Begin_VxD_Service_Table(PULLEYKINEMATICSTRANSFORMATIONDRV)
	VxD_Service( PULLEYKINEMATICSTRANSFORMATIONDRV_GetVersion )
End_VxD_Service_Table


#endif // ifndef __PULLEYKINEMATICSTRANSFORMATIONDRIVER_H__